# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the guess-words.bhdouglass package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: guess-words.bhdouglass\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-25 03:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/AboutPage.qml:26 ../qml/CategoryLanguagePage.qml:76
#: ../qml/CategoryListPage.qml:81
msgid "About"
msgstr ""

#: ../qml/AboutPage.qml:81 ../qml/CategoryListPage.qml:76
#: guess-words.desktop.in.h:1
msgid "Guess Words"
msgstr ""

#: ../qml/AboutPage.qml:88
msgid ""
"Pick a category, grab a partner, and get them to guess the words on the "
"screen before time runs out!"
msgstr ""

#: ../qml/AboutPage.qml:96 ../qml/HowToPlayDialog.qml:26
msgid "How to Play"
msgstr ""

#: ../qml/AboutPage.qml:109
msgid "Have an idea for a new category or suggestions for new words?"
msgstr ""

#: ../qml/AboutPage.qml:117
msgid "Submit a request on GitLab"
msgstr ""

#: ../qml/AboutPage.qml:125
msgid ""
"A Brian Douglass app, consider donating if you like it and want to see more "
"apps like it!"
msgstr ""

#: ../qml/AboutPage.qml:133
msgid "Donate"
msgstr ""

#: ../qml/CategoryLanguagePage.qml:104
msgid "Choose a language"
msgstr ""

#: ../qml/GamePage.qml:92
msgid "Get ready!"
msgstr ""

#: ../qml/GamePage.qml:126
msgid "Pass"
msgstr ""

#: ../qml/GamePage.qml:141
msgid "Next"
msgstr ""

#: ../qml/HowToPlayDialog.qml:34
msgid ""
"Grab a partner, pick a category, and try to get them to guess the words on "
"the screen before time runs out. Don't use rhyming words or spell out the "
"word, but gestures are encouraged. Once your partner guesses the word hit "
"the 'Next' button to get a new word. Feel free to tap the 'Pass' button if "
"you or your partner is struggling (as an extra challenge, try playing "
"without passing!). After time runs out, check your score and pass the phone "
"to your friend to start the next round. Have fun guessing!"
msgstr ""

#: ../qml/HowToPlayDialog.qml:38
msgid "Let's play!"
msgstr ""

#: ../qml/ResultsPage.qml:37
msgid "Results: %1"
msgstr ""
