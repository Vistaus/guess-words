/*
 * Copyright (C) 2020  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * guess-words is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Page {
    id: aboutPage
    header: PageHeader {
        id: header
        title: i18n.tr('About')

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        contentHeight: column.height + units.gu(4)
        clip: true

        ColumnLayout {
            id: column
            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                margins: units.gu(2)
            }

            spacing: units.gu(2)

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: ubuntuShapeIcon.height

                UbuntuShape {
                    id: ubuntuShapeIcon
                    anchors.centerIn: parent

                    width: units.gu(10)
                    height: units.gu(10)

                    image: Image {
                        source: Qt.resolvedUrl('../assets/logo.svg')

                        sourceSize {
                            width: ubuntuShapeIcon.width
                            height: ubuntuShapeIcon.height
                        }
                    }
                }
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Guess Words')
                horizontalAlignment: Label.AlignHCenter
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Pick a category, grab a partner, and get them to guess the words on the screen before time runs out!')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                text: i18n.tr('How to Play')
                color: UbuntuColors.orange
                onClicked: {
                    var popup = PopupUtils.open(howToPlayDialog)
                    popup.accepted.connect(function() {
                        PopupUtils.close(popup)
                    });
                }
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Have an idea for a new category or suggestions for new words?')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                text: i18n.tr('Submit a request on GitLab')
                color: UbuntuColors.orange
                onClicked: Qt.openUrlExternally('https://gitlab.com/bhdouglass/guess-words/issues')
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('A Brian Douglass app, consider donating if you like it and want to see more apps like it!')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                text: i18n.tr('Donate')
                color: UbuntuColors.orange
                onClicked: Qt.openUrlExternally('https://liberapay.com/bhdouglass')
            }
        }
    }

    HowToPlayDialog {
        id: howToPlayDialog
    }
}
